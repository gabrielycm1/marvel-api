import styled, { css } from "styled-components";
import { Link } from "react-router-dom";
import { shade } from "polished";

export const Container = styled.div`
  background-color: ${({ theme: { colors } }) => colors.gray_2};
  width: 100%;
  height: 45px;
  display: flex;
  align-items: center;
  justify-content: center;

  @media (max-width: 768px) {
    position: fixed;
    width: 250px;
    right: -100%;
    height: 100%;
    opacity: 0;
    z-index: 2;
    transition: all 1s;
    ${({ $showMenu }) =>
      $showMenu &&
      css`
        opacity: 1;
        right: 0;
      `}
  }
`;

export const List = styled.ul`
  width: 100%;
  height: 45px;
  display: flex;
  align-items: center;
  justify-content: center;
  gap: 30px;
  margin: 0;

  @media (max-width: 768px) {
    height: unset;
    justify-content: flex-start;
    align-items: flex-start;
    flex-direction: column;
  }
`;

export const ListItem = styled.li`
  list-style-type: none;
  a {
    color: ${({ theme: { colors }, $active }) =>
      $active ? colors.yellow_1 : colors.white_1};
    font-size: ${({ theme: { fontSize } }) => fontSize.biggest};
    font-weight: normal;
    font-family: "Marvel", sans-serif;
  }
  @media (max-width: 768px) {
    color: ${({ theme: { colors }, $active }) =>
      $active ? colors.yellow_1 : colors.white_1};
    list-style-type: circle;
  }
`;

export const Item = styled(Link)`
  text-transform: uppercase;
  text-decoration: none;
  cursor: pointer;
  &:hover {
    color: ${({ theme: { colors }, $active }) =>
      shade(0.1, $active ? colors.yellow_1 : colors.white_1)};
    border-bottom: 1px solid
      ${({ theme: { colors }, $active }) =>
        shade(0.1, $active ? colors.yellow_1 : colors.white_1)};
  }
`;
