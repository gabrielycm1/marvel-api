import { useLocation } from "react-router-dom";
import * as S from "./styles";

const items = [
  {
    path: "/",
    label: "Home",
  },
  {
    path: "/characters",
    label: "Characters",
  },
  {
    path: "/comics",
    label: "Comics",
  },
  {
    path: "/creators",
    label: "creators",
  },
  {
    path: "/events",
    label: "events",
  },
  {
    path: "/series",
    label: "series",
  },
];

export const Menu = ({ showMenu, setShow }) => {
  const { pathname } = useLocation();

  const isActiveRoute = (path) => {
    if (path === "/") return path === pathname;

    return pathname?.includes(path);
  };

  return (
    <S.Container $showMenu={showMenu}>
      <S.List>
        {items.map((item, index) => (
          <S.ListItem
            onClick={() => {
              if (showMenu) setShow(false);
            }}
            key={index}
            $active={isActiveRoute(item.path)}
          >
            <S.Item to={item.path} $active={pathname === item.path}>
              {item?.label}
            </S.Item>
          </S.ListItem>
        ))}
      </S.List>
    </S.Container>
  );
};
