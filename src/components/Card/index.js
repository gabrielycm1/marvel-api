import { useState } from "react";
import * as S from "./styles";

const Card = ({ onClick, thumbnail, title = "", name = "", fullName }) => {
  const [onHover, setOnHover] = useState(false);
  const image = `${thumbnail?.path}/portrait_incredible.${thumbnail?.extension}`;

  const principalInformation = title || name || fullName;
  return (
    <>
      <S.Container
        onMouseEnter={() => setOnHover((prev) => !prev)}
        onMouseLeave={() => setOnHover((prev) => !prev)}
        onClick={onClick}
      >
        <S.Image src={image} />
        {!!principalInformation && (
          <S.Description $onHover={onHover}>
            {<S.Text>{principalInformation}</S.Text>}
          </S.Description>
        )}
      </S.Container>
    </>
  );
};

export default Card;
