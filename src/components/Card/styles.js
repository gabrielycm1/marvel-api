import styled, { css } from "styled-components";

export const Container = styled.div`
  display: flex;
  flex: 2 1 auto;
  height: 200px;
  transition: transform 600ms;
  position: relative;
  cursor: pointer;

  &:focus,
  &:hover {
    transform: scale(1.2);
    z-index: 2;
  }

  @media (max-width: ${({ theme: { media } }) => media.tablet}) {
    width: 130px;
  }

  @media (max-width: ${({ theme: { media } }) => media.mobile_landscape}) {
    width: 110px;
  }
`;

export const Image = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

export const Description = styled.div`
  opacity: 0;
  position: absolute;
  bottom: 0;
  background-color: ${({ theme: { colors } }) => colors.black_1};
  padding: 10px;
  width: 100%;
  transition: opacity 600ms;
  ${({ $onHover }) =>
    $onHover &&
    css`
      opacity: 0.8;
    `}

  @media (max-width: ${({ theme: { media } }) => media.tablet}) {
    opacity: 0.6;
  }
`;

export const Text = styled.p`
  margin: 0;
  color: ${({ theme: { colors } }) => colors.white_1};
  font-family: "Marvel";
`;
