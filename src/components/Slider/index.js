import BearCarousel, { BearSlideItem } from "bear-react-carousel";

import arrow from "assets/arrow.svg";
import { useMedia } from "hooks/useMedia";

import * as S from "./styles";
import Card from "components/Card";

export const Slider = ({ data }) => {
  const { isSmall } = useMedia();

  const Arrow = ({ onClick, left }) => (
    <S.Arrow left={left} onClick={onClick}>
      <S.Icon src={arrow} />
    </S.Arrow>
  );

  const renderItem = data.map((item) => {
    const hasWiki = item?.urls?.filter((url) => url?.type === "wiki")[0];
    const hasDetail = item?.urls?.filter((url) => url?.type === "detail")[0];

    const onClick = () => {
      if (hasWiki) return window.open(hasWiki.url, "_blank");
      if (hasDetail) return window.open(hasDetail.url, "_blank");
    };
    return {
      key: item.id,
      children: (
        <BearSlideItem as="card">
          <Card {...item} onClick={onClick} />
        </BearSlideItem>
      ),
    };
  });

  return (
    <S.Container>
      <BearCarousel
        data={renderItem}
        isEnableLoop
        slidesPerView={isSmall ? 3.5 : 5.5}
        slidesPerGroup={isSmall ? 3 : 4}
        isEnableNavButton={isSmall ? false : true}
        spaceBetween={isSmall ? 10 : 20}
        renderNavButton={(toPrev, toNext) => (
          <>
            {<Arrow onClick={toNext} />}
            {<Arrow left onClick={toPrev} />}
          </>
        )}
      />
    </S.Container>
  );
};
