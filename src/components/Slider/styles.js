import styled, { css } from "styled-components";

export const Container = styled.div`
  position: relative;
  width: 100%;
  height: 230px;
  & > div {
    overflow: unset !important;
  }

  -webkit-tap-highlight-color: transparent;

  .bear-react-carousel__content {
    overflow: unset;
    .bear-react-carousel__slide-item {
      overflow: unset;
    }
  }
`;

export const Arrow = styled.div`
  background-color: ${({ theme: { colors } }) => colors.black_1};
  opacity: 0.6;
  display: flex;
  justify-content: center;
  align-items: center;
  position: absolute;
  width: 40px;
  height: 200px;
  cursor: pointer;
  pointer-events: all;
  z-index: 2;
  ${({ left }) =>
    left &&
    css`
      left: 0;
      box-shadow: 3px 0px 4px ${({ theme: { colors } }) => colors.black_1};
    `}
  ${({ left }) =>
    !left &&
    css`
      right: 0;
      box-shadow: -3px 0px 4px ${({ theme: { colors } }) => colors.black_1};
      img {
        transform: rotate(180deg);
      }
    `}
`;

export const Icon = styled.img``;
