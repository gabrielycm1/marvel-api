import styled from "styled-components";
import search from "assets/search.svg";

export const Input = styled.input`
  border: none;
  margin: 0;
  background: transparent;
  font-family: "Marvel";
  text-transform: uppercase;

  width: 0;
  outline: none;
  background: url(${search}) no-repeat scroll left;
  background-size: 15px;
  padding-left: 20px;
  transition: all 600ms;
  &:hover,
  &:focus,
  &:not(:placeholder-shown) {
    width: 200px;
    border-bottom: 1px solid #000;
  }
`;
