import * as S from "./styles";

export const Input = ({ placeholder, onChange, value }) => {
  return (
    <S.Input placeholder={placeholder} onChange={onChange} value={value} />
  );
};
