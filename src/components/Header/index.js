import { useState } from "react";

import logo from "assets/logo.svg";
import menu from "assets/menu.svg";
import { Menu } from "../Menu";

import * as S from "./styles";
export const Header = () => {
  const [showMenu, setShow] = useState(false);

  const toggleMenu = () => setShow((prev) => !prev);
  return (
    <>
      <S.Container>
        <S.Content>
          <S.Logo src={logo} />
        </S.Content>
      </S.Container>
      <S.MenuImage onClick={toggleMenu} src={menu} />
      <Menu showMenu={showMenu} setShow={setShow} />
    </>
  );
};
