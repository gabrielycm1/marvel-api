import styled from "styled-components";

export const Container = styled.header`
  width: 100%;
  background: linear-gradient(
    95.02deg,
    ${({ theme: { colors } }) => colors.gray_1} 0%,
    ${({ theme: { colors } }) => colors.black_2} 100%
  );
  filter: drop-shadow(
    0px 6px 6px ${({ theme: { colors } }) => colors.black_1}4f
  );

  display: flex;
  align-items: center;
  justify-content: center;

  @media (max-width: ${({ theme: { media } }) => media.tablet}) {
    position: fixed;
    z-index: 2;
  }
`;

export const Content = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 14px 30px;
  max-width: ${({ theme: { media } }) => media.laptops_landscape};
`;

export const Logo = styled.img`
  height: 52px;
  width: auto;
`;

export const MenuImage = styled.img`
  right: 40px;
  position: absolute;
  height: 22px;
  width: auto;
  top: 35px;
  z-index: 3;
  @media (min-width: ${({ theme: { media } }) => media.tablet}) {
    display: none;
  }
`;
