import { Header } from "../Header";

import * as S from "./styles";

export const Layout = ({ children }) => {
  return (
    <S.Container>
      <Header />
      {children}
      <footer></footer>
    </S.Container>
  );
};
