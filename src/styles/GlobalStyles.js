import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
  * {
    -webkit-box-sizing: border-box;
   -moz-box-sizing: border-box;
   box-sizing: border-box;
  }
  html, body, #root {
    margin: 0;
    padding: 0;
    height: 100vh;
    width: 100vw;
    overflow-x: hidden;
  }
`;

export default GlobalStyle;
