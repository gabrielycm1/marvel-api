import api from "./api.service";

const getMarvelData = async (path) => {
  try {
    const response = await api().get(path);
    return response.data;
  } catch (err) {
    console.warn("err::", err);
  }
};

export { getMarvelData };
