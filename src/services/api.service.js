import axios from "axios";
import md5 from "md5";

function getMd5() {
  const ts = new Date().getTime();

  const md5Hash = md5(
    ts +
      process.env.REACT_APP_MARVEL_PRIVATE_KEY +
      process.env.REACT_APP_MARVEL_PUBLIC_KEY
  );
  return { ts, md5Hash };
}

const api = () => {
  const { ts, md5Hash } = getMd5();

  return axios.create({
    baseURL: `https://gateway.marvel.com:443/v1/public`,
    params: {
      apikey: process.env.REACT_APP_MARVEL_PUBLIC_KEY,
      ts,
      hash: md5Hash,
    },
  });
};

export default api;
