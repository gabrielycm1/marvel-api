import { ThemeProvider } from "styled-components";
import { Provider } from "react-redux";

import Routes from "./pages/Routes";
import { GlobalStyle, Theme } from "./styles";
import { store } from "./store";
import "react-loading-skeleton/dist/skeleton.css";
import "bear-react-carousel/dist/index.css";

function App() {
  return (
    <Provider store={store}>
      <ThemeProvider theme={Theme}>
        <GlobalStyle />
        <Routes />
      </ThemeProvider>
    </Provider>
  );
}

export default App;
