import { BrowserRouter, Routes, Route } from "react-router-dom";

import { Home, AllCategories } from "./";

export default function Router() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/characters" element={<AllCategories />} />
        <Route path="/stories" element={<AllCategories />} />
        <Route path="/comics" element={<AllCategories />} />
        <Route path="/creators/" element={<AllCategories />} />
        <Route path="/events" element={<AllCategories />} />
        <Route path="/series" element={<AllCategories />} />
      </Routes>
    </BrowserRouter>
  );
}
