import React, { useEffect, useMemo } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";

import Skeleton from "react-loading-skeleton";

import { Layout, Slider } from "components";
import arrow from "assets/arrow_title.svg";

import * as S from "./styles";
import { marvelActions } from "store/ducks/marvel.duck";
import { useMedia } from "hooks/useMedia";

const resourcers = ["characters", "comics", "creators", "events", "series"];

export const Home = () => {
  const { error, ...data } = useSelector(({ marvel }) => marvel);
  const { isSmall } = useMedia();

  const dispatch = useDispatch();
  const navigate = useNavigate();

  useEffect(() => {
    resourcers.map((resource) =>
      dispatch(marvelActions.setMarvelData(resource))
    );
  }, [dispatch]);

  const categories = useMemo(() => Object.entries(data), [data]);

  return (
    <Layout>
      <S.Content>
        {categories.map((item, index) => (
          <S.Section key={index}>
            <S.Group onClick={() => navigate(`${item[0]}`)}>
              <S.SecTitle>{item[0]}</S.SecTitle>
              <S.Arrow src={arrow} />
            </S.Group>
            {!Array.isArray(item[1]) ? (
              <Slider data={item[1]?.results} />
            ) : (
              <S.Loading>
                <Skeleton
                  inline={true}
                  width={130}
                  height={200}
                  count={isSmall ? 3 : 7}
                />
              </S.Loading>
            )}
          </S.Section>
        ))}
      </S.Content>
    </Layout>
  );
};
