import styled from "styled-components";

export const Content = styled.div`
  padding: 30px;
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  max-width: ${({ theme: { media } }) => media.laptops_landscape};
  @media (max-width: ${({ theme: { media } }) => media.tablet}) {
    padding: 90px 0px 20px 20px;
  }
`;

export const Text = styled.h2``;

export const Section = styled.section`
  display: flex;
  flex-direction: column;
  width: 100%;
  overflow: hidden;
`;

export const SecTitle = styled.h3`
  font-family: "Marvel";
  font-style: italic;
  font-weight: 700;
  font-size: 18px;
  line-height: 15px;
  text-transform: uppercase;
`;

export const Card = styled.img`
  width: 130px;
  height: 200px;
`;

export const SliderContainer = styled.div`
  width: 200px;
  height: 200px;
`;

export const Loading = styled.div`
  span {
    display: flex;
    gap: 20px;
  }
`;

export const Group = styled.div`
  display: flex;
  width: min-content;
  gap: 10px;
  cursor: pointer;
`;

export const Arrow = styled.img``;
