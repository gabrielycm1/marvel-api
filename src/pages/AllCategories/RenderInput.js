import { useState } from "react";

import { useDebounce } from "hooks/useDebounce";
import { Input } from "components/Input";
import useDidMountEffect from "hooks/useDidMountEffect";

const searchType = (path) => {
  const types = {
    characters: `the start of name ${path}`,
    comics: `${path} title`,
    creators: `${path} first name`,
    events: `${path} name`,
    series: `${path} title`,
  };
  return types[path];
};

export const RenderInput = ({ path, setList }) => {
  const [inputValue, setInput] = useState("");

  const debounceInput = useDebounce(inputValue, 800);

  const onChange = ({ target: { value } }) => setInput(value);

  useDidMountEffect(() => {
    setList(debounceInput, path);
  }, [debounceInput]);

  useDidMountEffect(() => {
    setInput("");
  }, [path]);

  return (
    <Input
      placeholder={`search ${searchType(path)}`}
      onChange={onChange}
      value={inputValue}
    />
  );
};
