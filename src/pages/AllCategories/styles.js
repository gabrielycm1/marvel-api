import styled from "styled-components";

export const Content = styled.section`
  padding: 30px;
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  max-width: ${({ theme: { media } }) => media.laptops_landscape};
  @media (max-width: ${({ theme: { media } }) => media.tablet}) {
    padding: 100px 20px 20px 20px;
  }
`;

export const Title = styled.h3`
  text-transform: uppercase;
  font-family: "Marvel";
  margin: 0;
`;

export const List = styled.div`
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
  width: ${({ hasOne }) => (hasOne ? "300px" : "100%")};
  gap: 20px;
  margin-bottom: 20px;
`;

export const Loading = styled.p`
  text-transform: uppercase;
  font-family: "Marvel";
  font-size: 15px;
  width: 100%;
`;

export const CustomSkeleton = styled.div`
  span {
    display: flex;
    gap: 20px;
    flex-wrap: wrap;
  }
`;

export const SearchArea = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  gap: 10px;
  margin-bottom: 20px;
`;
