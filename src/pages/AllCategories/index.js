/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useRef, useState, useCallback, useMemo } from "react";
import { useLocation } from "react-router-dom";
import lodash from "lodash";

import { Card, Layout } from "components";
import { getMarvelData } from "services/marvel.service";

import * as S from "./styles";
import Skeleton from "react-loading-skeleton";
import { RenderInput } from "./RenderInput";

const initalPagination = {
  totalPages: 0,
  currentPage: 0,
  count: 0,
};

export const AllCategories = () => {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [isSearch, setIsSearch] = useState(false);
  const [page, setPage] = useState(0);
  const [actualPath, setActualPath] = useState("");
  const [pagination, setPagination] = useState(initalPagination);

  const { pathname } = useLocation();
  const loadingRef = useRef();

  const path = useMemo(() => pathname.replace("/", ""), [pathname]);

  const setInitialStates = () => {
    setActualPath(path);
    setData([]);
    setPagination(initalPagination);
    setPage(0);
    setIsSearch(false);
  };

  const setCategory = async () => {
    const offset = page * pagination.limit;
    if (!data.length) {
      setLoading(true);
    }
    const {
      data: { results, ...response },
    } = await getMarvelData(`${path}?offset=${offset}`);
    setLoading(false);
    setPagination({
      totalPages: response.total / response.limit,
      currentPage: response.offset / response.limit,
      limit: response.limit,
    });

    setData((oldResults) => lodash.uniqBy([...oldResults, ...results], "id"));
  };

  const setList = async (value, category) => {
    setLoading(true);
    setIsSearch(true);

    if (!value) {
      setIsSearch(false);
      setInitialStates();
      setCategory();
      return;
    }
    const searchType = {
      characters: "nameStartsWith",
      comics: "title",
      creators: "firstName",
      events: "name",
      series: "title",
    };

    const {
      data: { results },
    } = await getMarvelData(`${path}?${searchType[category]}=${value}`);
    setLoading(false);

    setData(results);
  };

  useEffect(() => {
    if (path !== actualPath) {
      setInitialStates();
      return;
    }
    if (isSearch) return;
    setCategory();
  }, [path, actualPath, page]);

  const hasPage = pagination.currentPage < pagination.totalPages;

  const loadmore = useCallback(
    (node) => {
      if (loadingRef.current) loadingRef.current.disconnect();
      loadingRef.current = new IntersectionObserver((entries) => {
        if (entries[0].isIntersecting && hasPage) {
          setPage((prev) => prev + 1);
        }
      });
      if (node) loadingRef.current.observe(node);
    },
    [hasPage]
  );

  const renderCards = (item, _path) => {
    const hasWiki = item?.urls?.filter((url) => url?.type === "wiki")[0];
    const hasDetail = item?.urls?.filter((url) => url?.type === "detail")[0];

    const onClick = () => {
      if (hasWiki) return window.open(hasWiki.url, "_blank");
      if (hasDetail) return window.open(hasDetail.url, "_blank");
    };
    return (
      <Card
        key={`${item.id}_${item.title || item.name}`}
        onClick={onClick}
        {...item}
      />
    );
  };

  return (
    <Layout>
      <S.Content>
        <S.SearchArea>
          <S.Title>{path}</S.Title>
          <RenderInput path={path} setList={setList} />
        </S.SearchArea>
        {loading ? (
          <S.CustomSkeleton>
            <Skeleton inline={true} width={150} height={200} count={6} />
          </S.CustomSkeleton>
        ) : !!data?.length ? (
          <S.List hasOne={data.length === 1}>
            {data?.map((item) => renderCards(item, pathname))}
          </S.List>
        ) : (
          <S.Loading>Nada Encontrado</S.Loading>
        )}
        {!isSearch && hasPage && !!data?.length && (
          <div ref={loadmore}>
            <S.Loading>Loading more...</S.Loading>
          </div>
        )}
      </S.Content>
    </Layout>
  );
};
