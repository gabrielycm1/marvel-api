import { getMarvelData } from "services/marvel.service";
const TYPES = {
  SET_DATA: "SET_DATA",
};

const INITIAL_STATE = {
  characters: [],
  comics: [],
  creators: [],
  events: [],
  series: [],
  error: "",
};

export const marvelReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case TYPES.SET_DATA:
      return { ...state, [action.payload.path]: action.payload.response.data };
    default:
      return state;
  }
};

export const marvelActions = {
  setMarvelData: (path) => async (dispatch) => {
    try {
      const response = await getMarvelData(path);
      dispatch({ type: TYPES.SET_DATA, payload: { path, response } });
    } catch (err) {
      dispatch({ type: TYPES.SET_ERROR, payload: err });
    }
  },
};
