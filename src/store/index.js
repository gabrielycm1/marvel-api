import { createStore, combineReducers, applyMiddleware } from "redux";
import thunk from "redux-thunk";

import { marvelReducer } from "./ducks/marvel.duck";

const rootReducer = combineReducers({
  marvel: marvelReducer,
});

const middleware = [thunk];

const store = createStore(rootReducer, applyMiddleware(...middleware));

export { store };
